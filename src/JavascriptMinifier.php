<?php

namespace NEkman\Javascript;

use NEkman\GoogleClosure\GoogleClosure;
use NEkman\GoogleClosure\FileCollection;

class JavascriptMinifier extends JavascriptCombiner {
	
	private $closure;
	
	public function __construct($path, GoogleClosure $closure) {
		parent::__construct($path);
		$this->closure = $closure;
	}

	protected function content(FileCollection $files) {
		return $this->closure->minify($files);
	}

}
