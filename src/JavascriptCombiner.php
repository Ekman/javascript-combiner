<?php

namespace NEkman\Javascript;

use NEkman\GoogleClosure\FileCollection;

class JavascriptCombiner {

	private $files;

	public function __construct($path) {
		$this->files = self::files($path);
	}

	/**
	 * Get all files in the given path
	 * @return array Array of JavaScript files
	 */
	private static final function files($path) {
		$dirIt = new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS);
		$objects = new \RecursiveIteratorIterator($dirIt, \RecursiveIteratorIterator::SELF_FIRST);

		$files = new FileCollection;
		foreach ($objects as $file) :
			if ($file->isDir()) :
				continue;
			endif;

			$files[] = $file->getPathname();
		endforeach;

		return $files;
	}

	public function sort(\Closure $sort) {
		$this->files->sort($sort);
		return $this;
	}

	public final function combine() {
		return $this->content($this->files);
	}

	protected function content(FileCollection $files) {
		$content = '';
		foreach ($files->iterator() as $file) :
			$content .= file_get_contents($file) . PHP_EOL;
		endforeach;
		return $content;
	}

}
